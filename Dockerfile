FROM openjdk:8

ARG NODE_VERSION=14
ARG PRINT_VERSIONS=0

RUN curl -sL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash -
RUN apt install nodejs -y
# RUN npm i -g --unsafe-perm turtle-cli
# RUN npm i -g --unsafe-perm expo-cli
# RUN npm i -g --unsafe-perm expo-modules-core
# RUN java -version && node -v && npm --version && turtle -V && expo --version
RUN npm i -g yarn

CMD ["/bin/bash", "-c", "echo 'Ready!'"]
